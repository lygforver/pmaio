#!/bin/sh

umount /mnt/pmem2-node0
umount /mnt/pmem3-node1

rmmod pmfs
insmod pmfs.ko measure_timing=0

sleep 1

mount -t pmfs -o init /dev/pmem2 /mnt/pmem2-node0
#mount -t pmfs -o init /dev/pmem3 /mnt/pmem3-node1

